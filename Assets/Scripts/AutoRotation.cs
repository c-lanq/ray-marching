using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MainCoroutine());
    }

    IEnumerator MainCoroutine()
    {
        float time = 0;
        while (time < 5)
        {
            transform.Rotate(0, 60 * (1 - Mathf.Abs(time - 2.5f) / 2.5f) * Time.deltaTime, 0, Space.World);
            time += Time.deltaTime;
            yield return null;
        }
        time = 0;
        yield return new WaitForSeconds(0.8f);
        while (time < 5)
        {
            transform.Rotate(0, -60 * (1 - Mathf.Abs(time - 2.5f) / 2.5f) * Time.deltaTime, 0, Space.World);
            time += Time.deltaTime;
            yield return null;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
