using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;

class VolumeFogCustomPass : CustomPass
{
    public Material material;
    private RTHandle volumeFogBuffer;
    private RTHandle volumeFogTempBuffer;
    private Shader invertDownSampleMaterialShader;
    private Material invertDownSampleMaterial;

    // It can be used to configure render targets and their clear state. Also to create temporary render target textures.
    // When empty this render pass will render to the active camera render target.
    // You should never call CommandBuffer.SetRenderTarget. Instead call <c>ConfigureTarget</c> and <c>ConfigureClear</c>.
    // The render pipeline will ensure target setup and clearing happens in an performance manner.
    protected override void Setup(ScriptableRenderContext renderContext, CommandBuffer cmd)
    {
        // Setup code here
        invertDownSampleMaterialShader = Shader.Find("FullScreen/VolumeFogInvertDownSample"); 
        invertDownSampleMaterial = CoreUtils.CreateEngineMaterial(invertDownSampleMaterialShader);

        volumeFogBuffer = RTHandles.Alloc(
            Vector2.one, TextureXR.slices, dimension: TextureXR.dimension,
            colorFormat: GraphicsFormat.R32G32B32A32_SFloat,
            // 此效果无需 Alpha
            useDynamicScale: true, name: "Outline Buffer"
        );

        volumeFogTempBuffer = RTHandles.Alloc(
            Vector2.one, TextureXR.slices, dimension: TextureXR.dimension,
            colorFormat: GraphicsFormat.R32G32_SFloat,
            // 此效果无需 Alpha
            useDynamicScale: true, name: "Outline Buffer"
        );
    }

    protected override void Execute(CustomPassContext ctx)
    {
        // Executed every frame for all the camera inside the pass volume.
        // The context contains the command buffer to use to enqueue graphics commands.

        // Render "volume fog low buffer"
        // 渲染所需网格，从而在轮廓缓冲区中应用轮廓效果
        CoreUtils.SetRenderTarget(ctx.cmd, volumeFogBuffer, ClearFlag.Color);
        CoreUtils.DrawFullScreen(ctx.cmd, material, shaderPassId: 0);

        // "denoise"
        //ctx.propertyBlock.SetTexture("_Buffer", volumeFogBuffer);
        //CoreUtils.SetRenderTarget(ctx.cmd, volumeFogTempBuffer, ClearFlag.None);
        //CoreUtils.DrawFullScreen(ctx.cmd, material, ctx.propertyBlock, shaderPassId: 1);

        // "blur"
        ctx.propertyBlock.SetTexture("_Buffer", volumeFogBuffer);
        CoreUtils.SetRenderTarget(ctx.cmd, ctx.cameraColorBuffer, ClearFlag.None);
        CoreUtils.DrawFullScreen(ctx.cmd, material, ctx.propertyBlock, shaderPassId: 3);

        //ctx.propertyBlock.SetTexture("_Buffer", volumeFogTempBuffer);
        //CoreUtils.SetRenderTarget(ctx.cmd, volumeFogBuffer, ClearFlag.None);
        //CoreUtils.DrawFullScreen(ctx.cmd, material, ctx.propertyBlock, shaderPassId: 3);

        // "up sample"
        //ctx.propertyBlock.SetTexture("_Buffer", volumeFogBuffer);
        //ctx.propertyBlock.SetTexture("_Buffer", volumeFogTempBuffer);
        //CoreUtils.SetRenderTarget(ctx.cmd, ctx.cameraColorBuffer, ClearFlag.None);
        //CoreUtils.DrawFullScreen(ctx.cmd, material, ctx.propertyBlock, shaderPassId: 2);
    }

    protected override void Cleanup()
    {
        // Cleanup code
        volumeFogBuffer.Release();
        volumeFogTempBuffer.Release();
    }
}